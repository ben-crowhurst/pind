Feature: Register domain name
		 In  order to register a domain name
		 As a registrar 
		 I want to register a domain record

		 Scenario: Register non-existent domain record
		 		   Given a domain record with name 'my.domain.com'
		 		   And addresses of '1.2.3.4, 5.6.7.8'
		 		   And a hash of 'ThisIsAbigFakeHashAndVeryInsecure'
		 		   When I POST the record to '/records'
		 		   Then I should see a status of 201 (Created)
		 		   And a location header of '/record/my.domain.com'

		 Scenario: Register pre-existing domain record
		 		   Given a domain record with name 'my.domain.com'
		 		   And addresses of '1.2.3.4, 5.6.7.8'
		 		   And a hash of 'ThisIsAbigFakeHashAndVeryInsecure'
		 		   When I POST the record to '/records'
		 		   Then I should see a status of 403 (Forbidden)

		 Scenario: Register malformed domain record
		 		   Given an empty domain record
		 		   When I POST the record to '/records'
		 		   Then I should see a status of 400 (Bad Request)