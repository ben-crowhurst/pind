var filesystem = require('fs');
var network    = require('./network');

exports.commit = function(record) {
	var filename = 'records.db/' + record.name;

	if (!filesystem.existsSync(filename)) {

		filesystem.mkdirSync('records.db/' + record.name);

		var descriptor = filesystem.openSync(filename + '/record.json', 'wx');
		filesystem.write(descriptor, JSON.stringify(record));
		filesystem.close(descriptor);
	} else {
		throw "error";
	}

	return record;
}

exports.remove = function(record) {
	filesystem.unlinkSync('records.db/' + record.name + '/record.json');

	filesystem.rmdirSync('records.db/' + record.name);

	return null;
}

exports.update = function(record) {
	var filename = 'records.db/' + record.name + '/record.json';

	if (filesystem.existsSync(filename)) {
		var descriptor = filesystem.openSync(filename, 'w');
		filesystem.write(descriptor, JSON.stringify(record));
		filesystem.close(descriptor);
	} else {
		filesystem.mkdirSync('records.db/' + record.name);

		var descriptor = filesystem.openSync(filename, 'wx');
		filesystem.write(descriptor, JSON.stringify(record));
		filesystem.close(descriptor);
	}
}

exports.find = function(record) {
	var result = null;

	if (network.isAddress(record.name)) {
		var records = filesystem.readdirSync('records.db');

		for (var index in records) {
			var entry = JSON.parse(filesystem.readFileSync('records.db/' + records[index] + '/record.json', 'utf8'));

			if (entry.addresses.indexOf(record.name) != -1) {
				result = entry;
				break;
			}
		}
	} else {
		result = JSON.parse(filesystem.readFileSync('records.db/' + record.name + '/record.json', 'utf8'));
	}

	return result;	
}

exports.findAll = function(start, stop) {
	var records = [];

	var filenames = filesystem.readdirSync('records.db/').sort();

	var start = (start) ? start : 0;
	var stop = (stop) ? stop : filenames.length - 1;

	for(var index = start; index <= stop; index++) {
		console.log(filenames[index]);

		var filename = 'records.db/' + filenames[index] + '/record.json';

		var record = JSON.parse(filesystem.readFileSync(filename, 'utf8'));

		records.push(record);
	}

	return records;
}


