var express = require('express');
var record = require('./route/record');
var records = require('./route/records');

var node = express();

node.use(express.bodyParser());

node.get('/records', records.read);
//node.get('records/:index', records.read);
node.get('/records/:start/:stop', records.read);
node.post('/records', records.create);

node.get('/record/:moniker', record.read);
node.put('/record/:moniker', record.update);
node.delete('/record/:moniker', record.delete);

node.listen(3000);

console.log('listening on port 3000');

//correct http status codes
//cucumber tests!
//ui
//remove sync calls
//http://nodejs.org/api/crypto.html#crypto_crypto_createcipheriv_algorithm_key_iv