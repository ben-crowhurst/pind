var filesystem = require('fs');
var repository = require('../repository');

exports.read = function(request, response) {
	var records = repository.findAll(request.params.start, request.params.stop);

	(records.length) ? response.send(records, 200) : response.send(404);
};

exports.create = function(request, response) {
	var record = repository.commit( request.body );

	if (record != null) {
		response.setHeader('Location', '/record/' + record.name);
		response.send(200);
	} else {
		response.send(404);
	}
}