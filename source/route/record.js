var repository = require('../repository');

exports.read = function(request, response) {
	var record = repository.find({name: request.params.moniker});

	(record != null) ? response.send(record, 200) : response.send(404);
};

exports.update = function(request, response) {
	var record = request.body;
	record.name = request.params.moniker;

	var result = repository.update(record);

	//only on create
	//http://stackoverflow.com/questions/797834/should-a-restful-put-operation-return-something
	//response.setHeader('Location', '/record/' + request.body.name);
	response.end();
};

exports.delete = function(request, response) {
	var record = request.body;
	record.name = request.params.moniker;

	var result = repository.remove(record);

	(result == null) ? response.send(result, 200) : response.send(404);
};