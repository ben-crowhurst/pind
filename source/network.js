exports.isAddress = function(value) {
	var ipv4 = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

	var ipv6 = /^$/;

	return (ipv4.test(value) || ipv6.test(value));
}